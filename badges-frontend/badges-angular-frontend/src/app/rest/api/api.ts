export * from './deleteBadge.service';
import { DeleteBadgeService } from './deleteBadge.service';
export * from './deleteBadge.serviceInterface'
export * from './getMetadata.service';
import { GetMetadataService } from './getMetadata.service';
export * from './getMetadata.serviceInterface'
export * from './putBadge.service';
import { PutBadgeService } from './putBadge.service';
export * from './putBadge.serviceInterface'
export * from './readBadge.service';
import { ReadBadgeService } from './readBadge.service';
export * from './readBadge.serviceInterface'
export const APIS = [DeleteBadgeService, GetMetadataService, PutBadgeService, ReadBadgeService];
