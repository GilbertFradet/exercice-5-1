package fr.cnam.foad.nfa035.badges.service.impl;

public enum ManagedImages {
    jpeg("image/jpeg"), jpg("image/jpeg"), png("image/png"), gif("image/gif");
    private String mimeType;
    ManagedImages(String mimeType) {
        this.mimeType = mimeType;
    }
    public  String getMimeType() {
        return mimeType;
    }
}
